# **WEEK 01 Journal**

>This is the first week of _semester 2_. I am excited about this semester as we get to learn whole lot of new stuff. This week Jeff introduced us to new educational website [to-bcs.nz][link1] that he created for students. Website is very useful with lots of material in it.  First week was all about getting familiar with **_[GUI programming][link2]_** content. Students were given four options of platform to learn **GUI Programming**. This include 
* asp net core
* electron
* unity
* swift. 
  >I am confused whether to go with asp net core or electron. I think I am going to choose asp net core.

[link1]: www.to-bcs.nz
[link2]: www.to-bcs.nz/COMP6001